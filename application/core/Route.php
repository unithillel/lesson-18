<?php


namespace application\core;

use Alex\traits\DBConnect;

class Route
{
    use DBConnect;

    static public function start(){
        $controllerName = 'home';
        $actionName = 'index';

        $routes = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
        
        if(!empty($routes[0])){
            $controllerName = $routes[0];
        }

        if(!empty($routes[1])){
            $actionName = $routes[1];
        }
        
        $controllerName = '\\application\\controllers\\'.ucfirst($controllerName).'Controller';
        $actionName = 'action'.ucfirst($actionName);
        


        $controller = new $controllerName;//new \application\controllers\PortfolioController
        $controller->$actionName(); // $controller->actionIndex(); $controller->actionContacts();
        
    }
}