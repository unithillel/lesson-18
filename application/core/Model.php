<?php


namespace application\core;


class Model
{
    protected static $db = null;

    /**
     * @return null
     */
    public static function getDb()
    {

        if(!self::$db){
//            self::$db = new \PDO('mysql:host='.getenv('DB_HOST').';dbname='.getenv('DB_NAME'),
//                getenv('DB_USER'), getenv('DB_PASSWORD'));
        }
        return self::$db;
    }
}