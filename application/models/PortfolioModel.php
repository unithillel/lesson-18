<?php
namespace application\models;

use application\core\Model;

class PortfolioModel extends Model
{
    static public function all(){
        $db = self::getDb();
        return [
            [
                'title' => 'Super website',
                'year' => 2020,
                'url' => 'https://google.com'
            ],
            [
                'title' => 'Lesson 16',
                'year' => 2020,
                'url' => 'https://lesson16.com'
            ]
        ];
    }
}